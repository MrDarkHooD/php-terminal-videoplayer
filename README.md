# terminal-videoplayer

## This is not ready, but it works.

I have successfully runned 128\*96px video with correct timing at 6fps.  
Technical limitation in terminator (with my system) seems to be 55fps at this resolution.  
Sound works at 16bit, mono and 4000 bitrate without problems, but way this system handles it is not yet working properly.  
I will do in future more testing to get this produce as good result as possible without it being just dump.

This is not optimal videoplayer, media files are not small and this project doesn't have real life use.  
I do this for fun and to test limitations of VGA, PHP and my own skills.

I use ffmpeg to make base files, but I am planning to do it later with PHP (with ffmpeg libary or something":D")

Basic idea:
* Video file first line has metadata
* all lines after that that do not have s: on beginning is one line of pixels in hex
* all lines that have s: on beginning have 1 second worth of raw sound data, decoded as hex

Problems that are present now:
* Piping sound doesn't work properly
* Timing is off, even "magic number" doesn't help

Command to make frames:
fmpeg -i video.mp4 -vf "scale=320:240,fps=12" frames/c01_%04d.jpeg

Command to make sound file:
ffmpeg -i video.mp4 -acodec pcm_s16le -ar 4000 -ac 1 sound16.wav

Header is first line of file, it has structure as follows:
* All values are separated by dot (.)
* All definitions are defined by name, following value without any separation
* Everything needs to be in right order as follows
  * v (versio)
  * fps
  * resx
  * resy
  * sound (boolean, 1 or 0)
* If sound is defined, bytes and bitrate must be defined:
  * b (bytes)
  * br (bitrate)
  * cc (channel count, can be defined as empty, default is 1)

Example header:  
LyVid.v0010.fps3.resx128.resy96.sound1.b8.br4000.cc1

Correct extension for files is .lvi and mimetype is video/lvi.
