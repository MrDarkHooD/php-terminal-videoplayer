<?php
error_reporting(E_ALL);

define('HEADER_DELIMITER', '.');

define('TYPE_INT','integer');
define('TYPE_STR', 'string');
define('TYPE_BOOL', 'boolean');



define('ARG_INPUT_VALUE', 1);
define('ARG_SET', 2);


define('ARG_INDEX', true);
define('ARG_REQUIRED_SET', true);

$allowedOptions = [
	"filePath" =>
	[
		"type"			=> TYPE_STR,
		"behavior"		=> ARG_INPUT_VALUE,
		"flags"			=> ["file", "input"],
		"Decription"	=> ""
	],
	"fileList" => [
		"type" => TYPE_STR,
		"behavior" => ARG_INPUT_VALUE,
		"flags" => ["list", "dir", "l", "d"]
	],
	"mute" => [
		"type" => TYPE_BOOL,
		"behavior" => ARG_SET,
		"flags" => ["m", "mute"]
	],
	"help" => [
		"type" => TYPE_BOOL,
		"behavior" => ARG_SET,
		"flags" => ["h", "help"]
	],
	"info" => [
		"type" => TYPE_BOOL,
		"behavior" => ARG_SET,
		"flags" => ["info", "i"]
	],
	"version" => [
		"type" => TYPE_BOOL,
		"behavior" => ARG_SET,
		"flags" => ["versio", "v"]
	],
];