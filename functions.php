<?php
declare(strict_types=1);

Class terminalHandle {

	public int $terminalWidth;
	public int $terminalHeight;
	public array $inputFlags;
	public array $allowedOptions;
	
	
	public function __construct(array $allowedOptions)
	{
		// https://stackoverflow.com/a/36658576
		$argx = (int) 0;
		$argc = (int) $_SERVER["argc"];
		$argv = (array) $_SERVER["argv"];

		while (++$argx < $argc && preg_match('/^-/', $argv[$argx]));

		(array) $arguments = (array) array_slice($argv, $argx);
		// End of borrowed code
		
		(string) $flag = null;
		(string) $value = null;

		$flags = [];

		while($input = array_shift($arguments))
		{
			$trimmedInput = ltrim($input, "-");
			
			if(!count($flags) && substr($input, 0, 1) != "-" )
			{
				$flags[array_key_first($allowedOptions)] = $input;
				continue;
			}
			
			if(substr($input, 0, 2) == "--" )
			{
				
				if(strpos($trimmedInput, "="))
				{
					
					list($flag, $value) = preg_split('/=/', $trimmedInput, 2);

				}
				else
				{
				
					$flag = $trimmedInput;
					$value = array_shift($arguments);
					
				}
				
				if($value == "")
				{
					echo $this->formatText("Arguments required for {$input} flag\n", ["red", "bold"]);
					die(2);
				}
				
				$flags[$flag] = $value;
				var_dump($flags);
				continue;
			}
			
			if( substr($input, 0, 1) == "-")
			{
				$flags[$trimmedInput] = (bool) true;
				continue;
			}

			if(count($flags) && substr($input, 0, 1) != "-" )
			{
				echo $this->formatText("Only first argument can be without flag.\n", ["red", "bold"]);
				die(2);
			}
			
		}
		
		$this->inputFlags = $flags;
		$this->allowedOptions = $allowedOptions;
		
	}

	public function intialize(): void
	{
		var_dump($this->inputFlags);
		foreach($this->inputFlags as $key => $flag)
		{
			/*
			
			array()[$key] =>
				[
					"type"			=> (int) TYPE_?, (INT, STR, BOOL
					"behavior"		=> (int) ARG_?, //SET, INPUT_VALUE
					"flags"			=> (array) ["?", "?"],
					"Decription"	=> (string) ""
				],
			
			*/
			if(!isset($this->allowedOptions[$key]))
			{
				echo $this->formatText("Flag {$key} not allowed.\n", ["red", "bold"]);
				die(2);
			}
			
			if($this->allowedOptions[$key]["type"] != gettype($flag))
			{
				echo $this->formatText("Flag {$key} has wrong type.\n", ["red", "bold"]);
				die(2);
			}
		}
		
	
		
		$this->terminalWidth	= (int) exec('tput cols');
		$this->terminalHeight	= (int) exec('tput lines');
		
		$this->clearScreen();
		return;
	}
	
	public function queryFlags( ?array $neededFlags = null ): array
	{
		return $this->inputFlags;
	}

	public function clearScreen(): void
	{
		echo chr(27).chr(91).'H'.chr(27).chr(91).'J';
		return;
	}
	
	public function disableCurisor(): void
	{
		exec("tput civis");
	}
	
	public function enableCurisor(): void
	{
		exec("tput cnorm");
	}
	
	public function formatText( string $text, array $options ): string
	{


/*		$formatList = [
			"red" => (string) "\033[31m$1\033[0m",
			"violet" => (string) "\033[35m$1\033[0m",
			"green" =>  (string) "\033[32m$1\033[0m",
			"cyan" => (string) "\033[96m$1\033[0m",
			"bold" => (string) "\033[1m$1\033[0m", // "\e[1m$text\e[0m"
			"italic" => (string) "\033[3m$1\033[0m",
			"underline" => (string) "\033[4m$1\033[0m",
			"strikethrough" => "\033[9m$1\033[0m"
		];*/
		
		$formatList = [
			"red" => (string) "\033[31m",
			"violet" => (string) "\033[35m",
			"green" =>  (string) "\033[32m",
			"cyan" => (string) "\033[96m",
			"bold" => (string) "\033[1m",
			"italic" => (string) "\033[3m",
			"underline" => (string) "\033[4m",
			"strikethrough" => "\033[9m"
		];

		(string) $formattedText = $text;
		
		foreach ( $formatList as $key => $value )
		{
			if ( in_array( $key, $options ) )
			{
				
				$formattedText = $value . $formattedText;
				
				$options = array_diff( $options, [ $key ] );
				
			}
		}
		
		if($formattedText !== $text)
		{
			
			$formattedText .= "\033[0m";
		}

		if( count($options) > 0 )
		{
			throw new Exception("Formatting options \"" . implode(", ", $options) . "\" not available.");
		}
		

		return $formattedText;
		
	}
}


define("LERROR_FILE_NOT_FOUND", 1);
define("LERROR_IMAGE_METADATA_VALIDATION_FAILURE", 2);
define("LERROR_SOUND_METADATA_VALIDATION_FAILURE", 3);

Class videoHandle {
	
	private int $delayBetweenFrames;
	private bool $muted;
	private bool $playerIntialized;
	private bool $paused;
	
	public function __construct()
	{
		$delayBetweenFrames = 0;
		$muted = false;
		$playerIntialized = false;
		$paused = true;
		$this->delayBetweenFrames = $delayBetweenFrames;
		$this->muted = $muted;
		$this->playerIntialized = $playerIntialized;
		$this->paused = $paused;
			
	}

	
	public function validateFile(string $filePath): array
	{

		$fileData = (array) [];
		$fileData["header"]	= (object) [];
		$fileData["bytes"]	= (object) [];
		
		//line 2
		$headerStringDetails = (array) [ // [ length, name, type ]
			["v", TYPE_INT],
			["fps", TYPE_INT],
			["resx", TYPE_INT],
			["resy", TYPE_INT],
			["sound", TYPE_BOOL]
		];

		$handle = fopen($filePath, 'r');
		$metaline = fgets($handle);
		
		echo $metaline."\n";
		
		preg_match(
			'/v(\d+)\.fps(\d+)\.resx(\d+)\.resy(\d+)\.sound(0|1)/',
			$metaline,
			$matches,
			PREG_UNMATCHED_AS_NULL
		);

		
		list(, $versio, $fps, $x, $y, $sound) = $matches;

		if(
			is_null($fps) ||
			is_null($x) ||
			is_null($y)
		)
		{
			$return = array
			(
				"error" => 1,
				"code" => LERROR_IMAGE_METADATA_VALIDATION_FAILURE,
				"dump" 	=> array
				(
					"fps" 	=> $fps,
					"x" 	=> $x,
					"y" 	=> $y
				)
			);
				
			return $return;
		}
		
		$metaData = array
		(
			"general" 	=> (object)[],
			"image" 	=> (object)[],
			"sound" 	=> (object)[]
		);
		
		$metaData["general"]->versio 	= (string)$versio;
		$metaData["image"]->fps 		= (int)$fps;
		$metaData["image"]->x 			= (int)$x;
		$metaData["image"]->y 			= (int)$y;
		$metaData["sound"]->enabled 	= (bool)$sound ?? false;

		if ( $sound )
		{
		
			preg_match(
				'/b(\d+)\.br(\d+)\.cc(\d+)/',
				$metaline,
				$matches,
				PREG_UNMATCHED_AS_NULL
			);

			list(, $bytes, $bitrate, $channels) = $matches;
			
			if(
				is_null($bytes) ||
				is_null($bitrate)
			)
			{
				$return = array
				(
					"error" => 1,
					"code" => LERROR_SOUND_METADATA_VALIDATION_FAILURE,
					"dump" 	=> array
					(
						"bytes" 	=> $bytes,
						"bitrate" 	=> $bitrate,
					)
				);

				return $return;
			}
			
			$metaData["sound"]->bytes		= (int)$bytes;
			$metaData["sound"]->bitrate		= (int)$bitrate;
			$metaData["sound"]->channels 	= (int)$channels ?? 1;

		}


		$metaData["general"]->linebreak = 2;
		
		$xlb = intval($metaData["image"]->y) * 2; //2 is linebreak bytes, too lazy to make it nice
		$frameBytes = (float) intval($metaData["image"]->x) * intval($metaData["image"]->y) * 6 + intval($xlb);

		$metaData["image"]->bytesPerFrame = $frameBytes;
		
		return $metaData;

	}
	
	public function intializePlayer(string $filePath): void
	{
		$fileData = $this->validateFile($filePath);
		
		$this->delayBetweenFrame = round(1000000 / $fileData["header"]->fps);
		
		$this->playerIntialized = true;
		$this->paused = true;
	}
	
	public function mute(): void
	{
		$this->muted = true;
	}
	
	public function unMute(): void
	{
		$this->muted = false;
	}

	public function startPlayer(): void
	{
		while($this->playerIntialized)
		{
			if($this->paused)
			{
				continue;
			}
			
			//...
			
		}
	}
	
	public function playPlayer(): void
	{
		$this->paused = false;
	}
	
	public function pausePlayer(): void
	{
		$this->paused = true;
	}
	
	public function hexToSoundOutput(string $hexData): void
	{
		if ( $this->muted )
		{
			return;
		}
		
		$hexData = ltrim($hexData, "s:");
		$hexData = rtrim($hexData, "\n");
		
		$pipePath = '/tmp/audio.pipe';

		// Start the play command in the background, reading from the named pipe
		//exec("play -r 4000 -b 16 -e signed-integer -t raw $pipePath > /dev/null 2>/dev/null &");

		
		
		//exec("echo -n '" . $hexData . str_repeat('0', 6*150) . "' | xxd -r -p | play -r 4000 -b 16 -e signed-integer -t raw - > /dev/null 2>/dev/null &");

		return;
	}
	
}



