<?php
require_once("config.php");
require_once("functions.php");

$allowedOptions = [
	"dir" =>
	[
		"type"			=> TYPE_STR,
		"behavior"		=> ARG_INPUT_VALUE,
		"flags"			=> ["dir", "d"],
		"Decription"	=> ""
	],
	"output" => [
		"type" => TYPE_STR,
		"behavior" => ARG_INPUT_VALUE,
		"flags" => ["output", "o"]
	],
	"fps" => [
		"type" => TYPE_INT,
		"behavior" => ARG_INPUT_VALUE,
		"flags" => ["fps"]
	],
	"sound" => [
		"type" => TYPE_STR,
		"behavior" => ARG_INPUT_VALUE,
		"flags" => ["s", "sound"]
	],
	"help" => [
		"type" => TYPE_BOOL,
		"behavior" => ARG_SET,
		"flags" => ["h", "help"]
	]
];

$terminal = new terminalHandle($allowedOptions);
$flags = $terminal->queryFlags();

#var_dump($flags);
#exit;

(string) $dir = ($flags["dir"]) ?? null;
(int) $fps = $flags["fps"] ?? 12;
(string) $output = ($flags["output"]) ?? null;
(string) $soundfile = ($flags["sound"]) ?? null;

echo "Welcome to make video/lvi file.\n";

if(is_null($dir))
	$dir = (string) readline("Frame directory: ");
var_dump($fps);
if(is_null($fps))
	$fps = (int) readline("Video fps: "); 

if(!is_dir($dir)) {
	echo $dir . " is not directory";
	die(2);
}

if(substr($dir, -1) != "/")
	$dir .= "/";

$scan = scandir($dir);

foreach($scan as $file)
{
	if(!is_file($dir.$file))
		continue;

	$firstFrame = $dir.$file;
	break;
}

list($width, $height) = getimagesize($firstFrame);
$mime = getimagesize($firstFrame)["mime"];

echo "Mimetype is " . $mime . "\n";
if($mime == "image/jpeg") $im = imagecreatefromjpeg($firstFrame);
if($mime == "image/png") $im = imagecreatefrompng($file);
if($mime == "image/webp") $im = imagecreatefromwebp($file);

// Lets go smol
$width  =   128; //exec('tput cols');
$height =   96;  //exec('tput lines'); //we can do 2 pixels for each char

list($width_orig, $height_orig) = getimagesize($firstFrame);
$ratio_orig = $width_orig / $height_orig;

$data = [];
$data["og_w"] = $width_orig;
$data["og_h"] = $height_orig;
$data["og_ratio"] = $ratio_orig;


if ($width/$height > $ratio_orig) {
	$width = $height*$ratio_orig;
} else {
	$height = $width/$ratio_orig;

}

$width = round($width);
$height = round($height); 

$data["resx"] = $width;
$data["resy"] = $height;

// Now he have metadata.

$data["fps"] = (int) str_pad(dechex($fps), 2, "0", STR_PAD_LEFT);;

$data["v"] = "0011";

if(is_null($soundfile))
	$soundfile = (string) readline("sound file: "); 

if(is_file($soundfile))
	$data["sound"] = 1;
else
	$data["sound"] = 0;

if(is_null($output))
	$output = (string) readline("Output file: "); 

if(is_file($output)) {
	echo $terminal->formatText("File \"$output\" exists!\n", ["red", "bold"]);
	if( readline("Continue writing? (y/n): ") != "y" )
		die(2);
	else
		unlink($output);
}

$writeStream = fopen($output, "w") or die("Unable to open file!");



$sf = fopen($soundfile, 'rb');
// Read the header of the WAV file to get the necessary information
$header = fread($sf, 44);
$sampleRate = unpack('V', substr($header, 24, 4))[1];
$bitsPerSample = unpack('v', substr($header, 34, 2))[1];
// Get number of channels (bytes 23-24 of the header)
$channels = unpack('v', substr($header, 22, 2))[1];

// Get bit depth (bytes 35-36 of the header)
$bitDepth = unpack('v', substr($header, 34, 2))[1];

$dataBytesPerSecond = ($sampleRate * $bitDepth * $channels) / $bitsPerSample;
$data["audio"]["sampleRate"] = $sampleRate;
$data["audio"]["bitsPerSample"] = $bitsPerSample;
$data["audio"]["BytesPerSecond"] = $dataBytesPerSecond;
$data["audio"]["BitDepth"] = $bitDepth;
$data["audio"]["Channels"] = $channels;

define('bps', $bitsPerSample);

$headerData = "LyVid.v".$data["v"].
	".fps".$data["fps"].
	".resx".$data["resx"].
	".resy".$data["resy"].
	".sound".$data["sound"].
	".b16".
	".br".$sampleRate.
	".cc".$channels.
	"\n";
fwrite($writeStream, $headerData);

$terminal->clearScreen();
echo "Header: " . $headerData;
print_r($data);
#die();
$frameCount = 0;
$data["fps"] = 12;
foreach(scandir($dir) as $file)
{
	
	if(!is_file($dir.$file))
		continue;
	
	$path = $dir.$file;
	
	#if($mime == "image/jpeg") $im = imagecreatefromjpeg($path);
	##if($mime == "image/png") $im = imagecreatefrompng($file);
	##if($mime == "image/webp") $im = imagecreatefromwebp($file);

	$im = imagecreatefromjpeg($path);
	$im = imagescale($im, $width, $height);
	
	if(is_int(($frameCount) / ($data["fps"]))) {
		// Sound data
		$soundData = fread($sf, $dataBytesPerSecond*2);

		$soundHexData = bin2hex($soundData);
		$lines = strval(exec('tput lines')-3);
		// Move cursor to row 5, column 10
		echo "\033[{$lines}H";
		echo $frameCount;
		#exec("echo -n '". $soundHexData . "' | xxd -r -p | play -r 2000 -b 8 -e signed-integer -t raw - > /dev/null 2>/dev/null &");
		fwrite($writeStream, strval("s:").$soundHexData . "\n");
	}
	
	for($y=0;$y<$height;$y++) {
		for($x=0;$x<$width;$x++) {
			$rgb = imagecolorat($im, $x, $y);

			list($r, $g, $b) = array_map(
				function($elem) {
					//Reader will fuck up if not enough bytes
					return str_pad(dechex($elem), 2, "0", STR_PAD_LEFT);
				}, [($rgb >> 16) & 0xFF, // Magic
					($rgb >> 8) & 0xFF, // Magic
					$rgb & 0xFF]); // Magic

			fwrite($writeStream, $r.$g.$b);

		}
		
		// Code will be bit smoother if line-by-line read
		fwrite($writeStream, "\n");
	}

	$lines = strval(exec('tput lines')-2);
	// Move cursor to row 5, column 10
	echo "\033[{$lines}H";
	echo $frameCount++ . " frames written, last image: ".$file.".\n";
}

fclose($writeStream);
fclose($sf);
echo "done";
die(0);

// "\x1b[38;2;$tr;$tg;$tbm\e[48;2;$br;$bg;$bbm▀\x1b[0m";

