#!/bin/php
<?php
declare(strict_types=1);

require_once("config.php");
require_once("functions.php");

$disableSound = false;

$terminal = new terminalHandle($allowedOptions);
$videoHandler = new videoHandle();

$terminal->intialize();
$flags = $terminal->queryFlags(["filePath"]);

//var_dump($flags);

$fileData = $videoHandler->validateFile($flags["filePath"]);
$terminal->disableCurisor();
//		echo "\x1b[38;2;{$r};{$g};{$b}m\e[48;2;{$r};{$g};{$b}m▀\x1b[0m";

#var_dump($fileData);
#exit;
(float) $sleeptime =  1000000 / intval($fileData["image"]->fps);
(float) $frameTime =  microtime(true);
(float) $frameStartTime =  microtime(true);
(float) $startTime =  microtime(true);

$handle = fopen($flags["filePath"], 'r');

$xbytes = (int) $fileData["image"]->x * 6;
$linesY = (int) $fileData["image"]->y;
$bitrate = (int) $fileData["sound"]->bitrate;
$sbyte = (int) $fileData["sound"]->bytes;

#die(strval($sleeptime));
#die();
$pipePath = '/tmp/audio.pipe';
if (!file_exists($pipePath))
{
    posix_mkfifo($pipePath, 0666);
}

$sbyte = 16;
$bitrate = 4000;

$command = "cat {$pipePath} |";
// reverse, postscript continuous hexdump style, bytes to output in while loop
$command .= "xxd -r -p -l $bitrate |";
# r = bitrate, b = bytes, -e encoding, t = type, - = read from stdin
$command .= "play -r $bitrate -b $sbyte -e signed-integer -t raw - >";
# in to the void it goes, don't wait
$command .= "/dev/null 2>/dev/null &";
	
if(!$disableSound)
	exec($command);


if(!$disableSound)
	$pipe = fopen($pipePath, 'w');

# Should stop code from waiting fwrite()
if(!$disableSound)
	stream_set_blocking($pipe, false);



$void = fgets($handle);
$currentLine = (int) 0;
$frameCount = (int) 0;

ob_implicit_flush(true);

while(1)
{

	$lineOne = fgets($handle);

	if(!$lineOne)
		break;
		
	if(str_split($lineOne, 2)[0] == "s:")
	{

		#$videoHandler->hexToSoundOutput(substr($lineOne, 2));
		if(!$disableSound)
			fwrite($pipe, substr($lineOne, 1, -1));

		$lineOne = fgets($handle);
	}
		
	if($currentLine >= $linesY)
	{
		#$toSleep = ($sleeptime-((float) ))*3.5;
		#echo $sleeptime."\n";
		#echo $frameStartTime."\n";
		#echo microtime(true)."\n";
		(float) $frameTime = (float) microtime(true) - $frameStartTime;
		$sleepTime = $sleeptime - $frameTime;
		usleep((int) $sleepTime);
		if (ob_get_length()) {
    		ob_end_flush();
    		flush();
		}
		#echo microtime(true)."\n";
		#die();
		#echo $toSleep."\n".$sleeptime;
		#die();
#		die(strval($toSleep));
		$frameStartTime = (float) microtime(true);
			
		$frameCount++;
		$currentLine = 0;
		
		echo "Frames: {$frameCount}\nTime passed: " .  strval((microtime(true) - $startTime)) . "\nLastframetime: {$frameTime}\nLast sleep time: ".$sleepTime.str_repeat(" ", 50)."\n";
		
		//Move cursor to top left
		echo "\033[H";

	}
		
	$lineOne = str_split($lineOne, 6);
	$lineTwo = fgets($handle);
	$lineTwo = str_split($lineTwo, 6);
#	$startTime = microtime(true);
	foreach ($lineOne as $rgbOne)
	{
		$rgbTwo = array_shift($lineTwo);
			
		if( mb_strlen($rgbOne) + mb_strlen(strval($rgbTwo)) < 12 )
		{
			continue;
		}

			
		$rgbOne = implode(";", array_map(function($elem) { return hexdec($elem); }, str_split(strval($rgbOne), 2)));
			
		$rgbTwo = implode(";", array_map(function($elem) { return hexdec($elem); }, str_split(strval($rgbTwo), 2)));

			echo "\x1b[38;2;{$rgbOne}m\e[48;2;{$rgbTwo}m▀\x1b[0m";
	}
		
	echo "\n";

	$currentLine += 2;
}
fclose($pipe);
fclose($handle);
$terminal->clearScreen();

var_dump([$startTime-microtime(true)/1000]);
?>